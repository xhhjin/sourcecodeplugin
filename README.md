# SourceCodePlugin for SyntaxHighlighter++ #

SourceCodePlugin for SyntaxHighlighter++是基于开源项目[Windows Live Writer Source Code plugin for SyntaxHighlighter](http://sourcecodeplugin.codeplex.com/)的2.0版本所做的，主要是针对Wordpress插件SyntaxHighlighter++，在Windows Live Writer中的提供高亮代码插入功能。

### 历史版本 ###

* 2.1.0.0，修改代码适应SyntaxHighlighter++；

### 如何安装 ###

1. 安装Windows Live Writer，然后关闭掉
1. 在[Downloads页面](https://bitbucket.org/xhhjin/sourcecodeplugin/downloads)下载对应的zip文件
1. 解压zip文件，将得到的dll放到Windows Live Writer的Plugins目录下
1. 打开Windows Live Writer，插入代码是选择Source Code Plug-in